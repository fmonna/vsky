<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>Calculation</name>
    <message>
        <location filename="vSky.py" line="734"/>
        <source>Temps d execution : {} secondes ---</source>
        <translation type="unfinished">Temps d’exécution : {} secondes ---</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="vSky.py" line="55"/>
        <source>{} Help</source>
        <translation type="unfinished">Aide de l’application {}</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="vSky.py" line="118"/>
        <source>Open Image</source>
        <translation type="unfinished">Ouvrir le MNT</translation>
    </message>
    <message>
        <location filename="vSky.py" line="87"/>
        <source>File</source>
        <translation type="unfinished">Fichier</translation>
    </message>
    <message>
        <location filename="vSky.py" line="90"/>
        <source>Open DEM</source>
        <translation type="unfinished">Ouvrir le MNT</translation>
    </message>
    <message>
        <location filename="vSky.py" line="92"/>
        <source>Open a DEM in tif format</source>
        <translation type="unfinished">Ouvrir un MNT au format TIFF</translation>
    </message>
    <message>
        <location filename="vSky.py" line="96"/>
        <source>Exit</source>
        <translation type="unfinished">Quitter</translation>
    </message>
    <message>
        <location filename="vSky.py" line="98"/>
        <source>Exit program</source>
        <translation type="unfinished">Quitter le programme</translation>
    </message>
    <message>
        <location filename="vSky.py" line="102"/>
        <source>Help</source>
        <translation type="unfinished">Aide</translation>
    </message>
    <message>
        <location filename="vSky.py" line="104"/>
        <source>General workflow described</source>
        <translation type="unfinished">Description de la procédure</translation>
    </message>
    <message>
        <location filename="vSky.py" line="107"/>
        <source>About...</source>
        <translation type="unfinished">À propos…</translation>
    </message>
    <message>
        <location filename="vSky.py" line="109"/>
        <source>Contributions</source>
        <translation type="unfinished">Contributions</translation>
    </message>
    <message>
        <location filename="vSky.py" line="112"/>
        <source>Status Bar</source>
        <translation type="unfinished">Statut</translation>
    </message>
</context>
<context>
    <name>Processing</name>
    <message>
        <location filename="vSky.py" line="215"/>
        <source>Path of the DEM:</source>
        <translation type="unfinished">Chemin du fichier MNT :</translation>
    </message>
    <message>
        <location filename="vSky.py" line="227"/>
        <source>DEM parameters</source>
        <translation type="unfinished">Paramètres du MNT</translation>
    </message>
    <message>
        <location filename="vSky.py" line="232"/>
        <source>Projection:</source>
        <translation type="unfinished">Projection :</translation>
    </message>
    <message>
        <location filename="vSky.py" line="237"/>
        <source>EPSG:</source>
        <translation type="unfinished">EPSG :</translation>
    </message>
    <message>
        <location filename="vSky.py" line="242"/>
        <source>Resolution (m/pix):</source>
        <translation type="unfinished">Résolution (m/pix) :</translation>
    </message>
    <message>
        <location filename="vSky.py" line="247"/>
        <source>Height:</source>
        <translation type="unfinished">Hauteur :</translation>
    </message>
    <message>
        <location filename="vSky.py" line="252"/>
        <source>Width:</source>
        <translation type="unfinished">Largeur :</translation>
    </message>
    <message>
        <location filename="vSky.py" line="259"/>
        <source>Preparation of the DEM</source>
        <translation type="unfinished">Préparation du MNT</translation>
    </message>
    <message>
        <location filename="vSky.py" line="263"/>
        <source>Prior smoothing (Gaussian kernel)</source>
        <translation type="unfinished">Lissage préalable (noyau gaussien)</translation>
    </message>
    <message>
        <location filename="vSky.py" line="283"/>
        <source>Radius (in pixels):</source>
        <translation type="unfinished">Rayon (en pixels) :</translation>
    </message>
    <message>
        <location filename="vSky.py" line="278"/>
        <source>Calculation parameters</source>
        <translation type="unfinished">Paramètres de calcul</translation>
    </message>
    <message>
        <location filename="vSky.py" line="290"/>
        <source>z exageration:</source>
        <translation type="unfinished">Exagération verticale</translation>
    </message>
    <message>
        <location filename="vSky.py" line="299"/>
        <source>VO</source>
        <translation type="unfinished">VO</translation>
    </message>
    <message>
        <location filename="vSky.py" line="302"/>
        <source>VO is systematically computed</source>
        <translation type="unfinished">VO est toujours calculé</translation>
    </message>
    <message>
        <location filename="vSky.py" line="303"/>
        <source>VOP</source>
        <translation type="unfinished">VOP</translation>
    </message>
    <message>
        <location filename="vSky.py" line="305"/>
        <source>VON</source>
        <translation type="unfinished">VON</translation>
    </message>
    <message>
        <location filename="vSky.py" line="313"/>
        <source>Output parameters</source>
        <translation type="unfinished">Paramètres de sortie</translation>
    </message>
    <message>
        <location filename="vSky.py" line="318"/>
        <source>8-bits output</source>
        <translation type="unfinished">Sortie 8 bits</translation>
    </message>
    <message>
        <location filename="vSky.py" line="322"/>
        <source>Launch calculation</source>
        <translation type="unfinished">Lancer le calcul</translation>
    </message>
    <message>
        <location filename="vSky.py" line="326"/>
        <source>Calculate</source>
        <translation type="unfinished">Calculer</translation>
    </message>
    <message>
        <location filename="vSky.py" line="328"/>
        <source>Run the calculation &lt;i&gt;Calculate&lt;/i&gt;</source>
        <translation type="unfinished">Lancer le calcul</translation>
    </message>
    <message>
        <location filename="vSky.py" line="331"/>
        <source>Use GPU</source>
        <translation type="unfinished">Utiliser le GPU</translation>
    </message>
</context>
</TS>
