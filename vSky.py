import sys, os
import time
from PyQt5 import QtWidgets, QtCore
QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)
QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps, True)
from PyQt5.QtWidgets import (QMainWindow,
        QDialog,
        QApplication, 
        QLabel,
        QFileDialog,
        QAction,
        QCheckBox,
        QVBoxLayout,
        QHBoxLayout,
        QWidget,
        QTextBrowser,
        QPushButton,
        QFormLayout,
        QGroupBox,
        QSpinBox,
        QMessageBox,
        QProgressBar)
from PyQt5.QtGui import QPixmap, QImage, QIcon
from PyQt5.QtCore import Qt, QUrl, QThread, pyqtSignal, QLocale, QTranslator
from osgeo import gdal, osr
import numpy as np
from PIL import Image, ImageQt
from scipy.signal import fftconvolve
import qrc_resources
try:
    import cupy as cp
    CUDA_AVAILABLE = True
except ModuleNotFoundError:
    CUDA_AVAILABLE = False


class Help(QDialog):

    def __init__(self, page, parent=None):
        super(Help, self).__init__(parent)
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setAttribute(Qt.WA_GroupLeader)

        self.pageLabel = QLabel()

        self.textBrowser = QTextBrowser()

        layout = QVBoxLayout()
        layout.addWidget(self.textBrowser)
        self.setLayout(layout)

        self.textBrowser.setSearchPaths([":/"])
        self.textBrowser.setSource(QUrl(page))
        self.resize(400, 600)
        self.setWindowTitle(self.tr("{} Help").format(
                QApplication.applicationName()))


class MainWindow(QMainWindow):
       
    def __init__(self):
        
        super().__init__()
        
        #size of the window
        
        self.title = 'vSky - Volumetric Open Sky'
        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon(':/icon.png'))
        
        
        # Image logo at the opening of the program
        
        self.central_widget = QWidget()               
        self.setCentralWidget(self.central_widget)
        
        lay = QVBoxLayout(self.central_widget)
        label_image = QLabel(self)
        pixmap = QPixmap(':/icon.png')
        label_image.setPixmap(pixmap)
        label_image.setAlignment(Qt.AlignCenter)
        lay.addWidget(label_image)

        # creation of the menu bar
        
        menubar = self.menuBar()
        file_menu = menubar.addMenu(self.tr('File'))
        info_menu = menubar.addMenu('?')
        
        openAction = QAction(self.tr('Open DEM'), self)  
        openAction.setShortcut('Ctrl+O')
        openAction.setStatusTip(self.tr("Open a DEM in tif format"))
        openAction.triggered.connect(self.open_image) 
        file_menu.addAction(openAction)

        close_action = QAction(self.tr('Exit'), self)  
        close_action.setShortcut('Ctrl+Q')
        close_action.setStatusTip(self.tr("Exit program"))
        close_action.triggered.connect(self.close) 
        file_menu.addAction(close_action)
        
        Info_action = QAction(self.tr('Help'), self)  
        Info_action.triggered.connect(self.click_help) 
        Info_action.setStatusTip(self.tr("General workflow described"))
        info_menu.addAction(Info_action)
        
        about_action = QAction(self.tr('About...'), self)  
        about_action.triggered.connect(self.click_about) 
        about_action.setStatusTip(self.tr("Contributions"))
        info_menu.addAction(about_action)
        
        self.statusBar().showMessage(self.tr('Status Bar'))
        
        self.showMaximized()
      
    def open_image(self):
        
        image_path,_= QFileDialog.getOpenFileName(self, self.tr('Open Image'),'./','Image Files(*.tif)')
        
        if image_path != "":      # if open image is canceled return to the main window, otherwise proceed
            
            im_originale = gdal.Open(image_path)
            proj = im_originale.GetProjection()
            srs = osr.SpatialReference(wkt = proj)
        
            projection = srs.GetAttrValue('projcs')
            
            if (projection is not None):
                window = Processing(image_path)
                self.setCentralWidget(window)
                            
            else:
                
                M1 = 'Only projected DEMs can be treated!'
                M2 = '\n\nFor DEMs just scaled, apply any metric projection first.'
                M3 = '\n\nYou can use, for instance, RGF93 / Lambert 93 (EPSG: 2154).'
                message = M1 + M2 + M3            
                QMessageBox.critical(self, "Error", message)

        
    def click_help(self):

        form = Help('index.html', self)
        form.show()
    
    def click_about(self):

        M1 = 'For more information contact Fabrice Monna: Fabrice.Monna@u-bourgogne.fr'
        M2 = '\n\nor Tanguy Rolland: Tanguy.Rolland@u-bourgogne.fr'
        M3 = '\n\nThe software has been programmed with Python 3.7 with the help of the PyQt5, sys, numpy, PIL, and osgeo libraries.'
        M4 = '\n\nVersion 1.0, 2020'
        
        message = M1 + M2 + M3 + M4
        QMessageBox.about(self, "General workflow", message)
        

class AspectRatioPixmapLabel(QLabel):
    def __init__(self, parent):
        super(AspectRatioPixmapLabel, self).__init__(parent)
        self.pix = None
        self.setMinimumSize(1, 1)
        self.setScaledContents(False)

    def setPixmap(self, pixmap):
        self.pix = pixmap
        super().setPixmap(self.scaledPixmap())

    def heightForWidth(self, width):
        return self.height() if self.pix else self.pix.height() * width / self.pix.width()

    def sizeHint(self):
        width = self.width()
        return QtCore.QSize(width, self.heightForWidth(width))

    def scaledPixmap(self):
        scaled = self.pix.scaled(self.size() * self.devicePixelRatioF(), Qt.KeepAspectRatio, Qt.SmoothTransformation)
        scaled.setDevicePixelRatio(self.devicePixelRatioF())
        return scaled

    def resizeEvent(self, event):
        if self.pix:
            super().setPixmap(self.scaledPixmap())


class Processing(QWidget):
      
    def __init__(self, image_path):
        
        super().__init__()
        self.prep_blur_radius = 2
        self.exageration_value = 1
        self.checked_vo = True
        self.checked_vop = False
        self.checked_von = False
        self.image_folder, self.image_name = os.path.split(image_path)
        self.setup_ui()
        self.extract_image(image_path)

    def setup_ui(self):
        # create and set layout to place widgets
        primary_layout = QHBoxLayout(self)
        self.setLayout(primary_layout)

        image_box = QGroupBox(self)
        secondary_left_layout = QVBoxLayout(image_box)
        image_box.setLayout(secondary_left_layout)

        self.image = AspectRatioPixmapLabel(image_box)
        secondary_left_layout.addWidget(self.image, stretch=3)

        image_location = QWidget(image_box)
        image_location_layout = QFormLayout()
        image_location.setLayout(image_location_layout)
        image_location_label = QLabel()
        image_location_label.setText(self.tr('Path of the DEM:'))
        self.image_location_value = QLabel()
        self.image_location_value.setText(os.path.join(self.image_folder, self.image_name))
        image_location_layout.addRow(image_location_label, self.image_location_value)
        secondary_left_layout.addWidget(image_location)

        right_block = QWidget(self)
        right_block.setMinimumSize(300,600)
        secondary_right_layout = QVBoxLayout(right_block)
        primary_layout.addWidget(image_box, stretch=2)
        primary_layout.addWidget(right_block, stretch=1)

        data_box = QGroupBox(self.tr('DEM parameters'))
        data_form = QFormLayout(data_box)
        data_box.setLayout(data_form)
        
        data_proj_label = QLabel(self)
        data_proj_label.setText(self.tr('Projection:'))       
        self.data_proj_value = QLabel(self)       
        data_form.addRow(data_proj_label, self.data_proj_value)

        data_epsg_label = QLabel(self)
        data_epsg_label.setText(self.tr('EPSG:'))       
        self.data_epsg_value = QLabel(self)       
        data_form.addRow(data_epsg_label, self.data_epsg_value)

        data_res_label = QLabel(self)
        data_res_label.setText(self.tr('Resolution (m/pix):'))       
        self.data_res_value = QLabel(self)       
        data_form.addRow(data_res_label, self.data_res_value)
        
        data_height_label = QLabel(self)
        data_height_label.setText(self.tr('Height:'))       
        self.data_height_value = QLabel(self)       
        data_form.addRow(data_height_label, self.data_height_value)
        
        data_width_label = QLabel(self)
        data_width_label.setText(self.tr('Width:'))       
        self.data_width_value = QLabel(self)
        data_form.addRow(data_width_label, self.data_width_value)
        

        secondary_right_layout.addWidget(data_box)

        prep_box = QGroupBox(self.tr('Preparation of the DEM'))
        #prep_box.setFrameStyle(QFrame.Panel | QFrame.Plain)
        prep_box_layout = QFormLayout()

        self.check_blur = QCheckBox(self.tr("Prior smoothing (Gaussian kernel)"),self)
        self.check_blur.setChecked(False)
        self.check_blur.toggled.connect(self.toggle_preprocessing)
        self.prep_blur_label = QLabel(prep_box)
        self.prep_blur_label.setText(self.tr('Radius (in pixels):'))
        self.prep_blur_label.setEnabled(False)
        self.prep_blur_spin = QSpinBox(self)
        self.prep_blur_spin.setRange(1,100)
        self.prep_blur_spin.setValue(self.prep_blur_radius)
        self.prep_blur_spin.setEnabled(False)                    

        prep_box_layout.addRow(self.check_blur)  
        prep_box_layout.addRow(self.prep_blur_label,self.prep_blur_spin) 
        prep_box.setLayout(prep_box_layout)

        calc_param_box = QGroupBox(self.tr('Calculation parameters'))
        calc_param_layout = QFormLayout()
        calc_param_box.setLayout(calc_param_layout)

        calc_param_radius_label = QLabel(self)
        calc_param_radius_label.setText(self.tr('Radius (in pixels):'))
        self.calc_param_radius_spin = QSpinBox(self)
        self.calc_param_radius_spin.setRange(2,100)
        self.calc_param_radius_spin.setValue(5)
        calc_param_layout.addRow(calc_param_radius_label, self.calc_param_radius_spin)

        calc_param_exageration_label = QLabel(self)
        calc_param_exageration_label.setText(self.tr('z exageration:'))
        self.calc_param_exageration = QSpinBox(self)
        self.calc_param_exageration.setRange(1,1000000)
        self.calc_param_exageration.setValue(self.exageration_value)
        calc_param_layout.addRow(calc_param_exageration_label, self.calc_param_exageration)
        
        calc_param_method = QWidget(calc_param_box)
        calc_param_method_layout = QHBoxLayout()
        calc_param_method.setLayout(calc_param_method_layout)
        self.calc_param_vo = QCheckBox(self.tr("VO"), self)
        self.calc_param_vo.setChecked(self.checked_vo)
        self.calc_param_vo.setEnabled(False)
        self.calc_param_vo.setToolTip (self.tr('VO is systematically computed'))       
        self.calc_param_vop = QCheckBox(self.tr("VOP"), self)
        self.calc_param_vop.setChecked(self.checked_vop)
        self.calc_param_von = QCheckBox(self.tr("VON"), self)
        self.calc_param_von.setChecked(self.checked_von)
        calc_param_method_layout.addWidget(self.calc_param_vo)
        calc_param_method_layout.addWidget(self.calc_param_vop)
        calc_param_method_layout.addWidget(self.calc_param_von)
        calc_param_method_layout.addStretch()
        calc_param_layout.addRow(calc_param_method)

        output_box = QGroupBox(self.tr('Output parameters'))
        output_layout = QFormLayout()
        output_box.setLayout(output_layout)
        #output_box.setFrameStyle(QFrame.Panel | QFrame.Plain)
        
        self.check_8_bits = QCheckBox(self.tr("8-bits output"),output_box)
        self.check_8_bits.setChecked(True)
        output_layout.addRow(self.check_8_bits)

        launch_box = QGroupBox(self.tr('Launch calculation'))
        launch_layout = QVBoxLayout()
        launch_box.setLayout(launch_layout)
        #launch_box.setFrameStyle(QFrame.Panel | QFrame.Plain)
        self.bt_calc = QPushButton(self.tr("Calculate"), launch_box)
        self.bt_calc.setFixedWidth(100)
        self.bt_calc.setToolTip(self.tr("Run the calculation <i>Calculate</i>"))      
        self.bt_calc.clicked.connect(self.click_calc)
        launch_layout.addWidget(self.bt_calc)
        self.toggle_calc_mode = QPushButton(self.tr("Use GPU"), launch_box)
        self.toggle_calc_mode.setCheckable(True)
        self.toggle_calc_mode.setEnabled(CUDA_AVAILABLE)
        launch_layout.addWidget(self.toggle_calc_mode)
        self.progress = QProgressBar(launch_box)
        self.progress.setGeometry(0, 0, 300, 25)
        self.progress.setMaximum(100)
        launch_layout.addWidget(self.progress)

        secondary_right_layout.addWidget(prep_box)
        secondary_right_layout.addWidget(calc_param_box)
        secondary_right_layout.addWidget(output_box)
        secondary_right_layout.addStretch()
        secondary_right_layout.addWidget(launch_box)

    def extract_image(self, image_path):
              
        # destroy result windows if they exist
        if hasattr(self,'self.win0'):
            self.win0.close()
        
        if hasattr(self,'self.win1'):
            self.win1.close()
            
        if hasattr(self,'self.win2'):
            self.win2.close()

        if hasattr(self,'self.win3'):
            self.win3.close()            
        
        # extraction of name of the file, georeferencement, band…
        
        self.im_originale = gdal.Open(image_path)
        self.cols = self.im_originale.RasterXSize
        self.rows = self.im_originale.RasterYSize                            
        _, self.x_res, _, _, _, self.y_res = self.im_originale.GetGeoTransform()
        
        proj = self.im_originale.GetProjection()
        srs = osr.SpatialReference(wkt=proj)    
        projection = srs.GetAttrValue('projcs')
        self.epsg = srs.GetAttrValue('AUTHORITY',1)

        # transformation as an image, extraction of size, elimination of nan, contrast improvement
        
        im_numpy = self.im_originale.ReadAsArray()                                              
        width = im_numpy.shape[0]
        height = im_numpy.shape[1]
        self.wherenan, self.image_final_npy = tranformImage(im_numpy)
        image8bits = array_image(self.image_final_npy, 2.5, 97.5)
        
        im8 = Image.fromarray(image8bits)
        imQt = QImage(ImageQt.ImageQt(im8))
        self.original_pixmap = QPixmap.fromImage(imQt)

        self.image.setPixmap(self.original_pixmap)

        # Replacement of values (by convention) by Nan
        
        self.image_final_npy = np.nan_to_num(self.image_final_npy)
        
        # layout construction
        
        self.data_proj_value.setText(projection)
        self.data_epsg_value.setText(self.epsg)
        self.data_res_value.setText(f'{self.x_res}')
        self.data_height_value.setText(str(height))
        self.data_width_value.setText(str(width))
        
    def toggle_preprocessing(self):
        
        "Set variables and states for preprocessing"
        if self.check_blur.isChecked():
            self.prep_blur_spin.setEnabled(True)
            self.prep_blur_label.setEnabled(True)
        else:
            self.prep_blur_spin.setEnabled(False)
            self.prep_blur_label.setEnabled(False)

    def click_calc(self):
                     
#         close result windows if they already exist  
               
        if hasattr(self,'self.win0'):
            self.win0.close()
        
        if hasattr(self,'self.win1'):
            self.win1.close()
            
        if hasattr(self,'self.win2'):
            self.win2.close()
            
        if hasattr(self,'self.win3'):
            self.win3.close()            
        
        # contruction of a grid of pixels around the point of interest (i.e. grid)
        radius_value = self.calc_param_radius_spin.value()
        
        x = np.arange(-radius_value, radius_value + 1, dtype = int)  
        y = np.arange(-radius_value, radius_value + 1, dtype = int)  
        grid = np.stack(np.meshgrid(x, y), -1).reshape(-1, 2) 
        distance = np.sqrt(grid[:,0]**2 + grid[:,1]**2) 
        self.d = distance[distance < radius_value] 
        self.grid = grid[distance < radius_value]   
        
        # vertical correction to apply
        exageration_value = self.calc_param_exageration.value()
        
        self.h_vert_cor = np.sqrt(radius_value**2 - self.d**2) * self.x_res / exageration_value      
        
        # define max for progression bar
        
        self.progress.setMaximum(self.d.size-1)

        checked_vop = self.calc_param_vop.isChecked()
        checked_von = self.calc_param_von.isChecked()
        checked_cuda = self.toggle_calc_mode.isChecked()
        # calculation 
        if self.check_blur.isChecked():    # blurring option 
            self.calc = Calculation(self.grid, self.d, self.h_vert_cor,
                    blurring(self.image_final_npy, self.prep_blur_spin.value()), checked_vop, checked_von, checked_cuda)
            
        else:                      #no blur
            self.calc = Calculation(self.grid, self.d, self.h_vert_cor,
                            self.image_final_npy, checked_vop, checked_von, checked_cuda)        
            
        self.calc.countChanged.connect(self.onCountChanged) 
        self.calc.start()       
        
        
    def onCountChanged(self, value):
            
        self.progress.setValue(value)
        
        #print(self.d.size, self.progress.value())        
        
        # when calculation is done, save and show results
        
        if self.progress.value() == (self.d.size -1):
            
            shift = 0
            self.progress.setValue(0) # set the progression bar to zero
            
            self.out_vo = self.calc.large_vo[:,:,0]  / (2 * self.h_vert_cor.sum())  # collect result for VO
            self.out_vo[self.wherenan == True] = 'nan' 
            self.nm = 'VO'       
            self.im = self.out_vo
            if CUDA_AVAILABLE and isinstance(self.im, cp.ndarray):
                self.im = cp.asnumpy(self.im)
            self.saveImage()  
            self.win0 = PopupWin(self.im, shift, self.newname)    
            self.win0.show()
            
            if self.calc_param_vop.isChecked():
                
                shift = shift + 50
                self.out_vop = self.calc.large_vop[:,:,0] / self.h_vert_cor.sum()    # result for vop
                self.out_vop[self.wherenan == True] = 'nan' 
                self.nm = 'VOP'
                self.im = self.out_vop
                if CUDA_AVAILABLE and isinstance(self.im, cp.ndarray):
                    self.im = cp.asnumpy(self.im)
                self.saveImage()                   
                self.win1 = PopupWin(self.im, shift, self.newname)  
                self.win1.show()
                
            if self.calc_param_von.isChecked():
                
                shift = shift + 50
                self.out_von = self.calc.large_von[:,:,0] / self.h_vert_cor.sum()   # result for von
                self.out_von[self.wherenan == True] = 'nan' 
                self.nm = 'VON'
                self.im = self.out_von
                if CUDA_AVAILABLE and isinstance(self.im, cp.ndarray):
                    self.im = cp.asnumpy(self.im)
                self.saveImage()
                self.win2 = PopupWin(self.im, shift, self.newname)        
                self.win2.show()

            if self.calc_param_von.isChecked() & self.calc_param_vop.isChecked() :
                
                shift = shift + 50
                self.nm = 'RGB_combine'
                
                if CUDA_AVAILABLE and isinstance(self.out_vo, cp.ndarray) and isinstance(self.out_vop, cp.ndarray) and isinstance(self.out_von, cp.ndarray):
                    self.out_vo = cp.asnumpy(self.out_vo)  
                    self.out_vop = cp.asnumpy(self.out_vop) 
                    self.out_von = cp.asnumpy(self.out_von) 
                    
                self.saveImageRGB()
                self.win3 = PopupWinRGB(self.out_image, shift, self.newname)        
                self.win3.show()

    def saveImage(self):            
        
        ###### write results  
        # construction of the name of the file
        
        exageration_value = self.calc_param_exageration.value()
        radius_value = self.calc_param_radius_spin.value()
        blur_value = self.prep_blur_spin.value()
        if exageration_value == 1:        
            exagere = ''
        else:
            exagere = '_Z-exag='+str(exageration_value)
        
        if self.check_blur.isChecked():
            blur = '_smooth_r='+str(blur_value)
        else:
            blur = ''

        basename = os.path.splitext(self.image_name)[0]
        self.newname = os.path.join(self.image_folder, f'{basename}_{self.nm}_r={radius_value}{blur}{exagere}.tif')

        # Save as geotif
        
        cols = self.im_originale.RasterXSize
        rows = self.im_originale.RasterYSize                            
        bands = self.im_originale.RasterCount
        gt = self.im_originale.GetGeoTransform()
        proj = self.im_originale.GetProjection()

        driver = gdal.GetDriverByName('GTIFF')    
        driver.Register()

        output = driver.Create(self.newname, cols, rows, bands, gdal.GDT_Float32)
        output.SetGeoTransform(gt)
        output.SetProjection(proj)
        outBand = output.GetRasterBand(1)
        outBand.WriteArray(self.im, 0, 0)
        output = None
        outBand = None   
        
        # Construction of visual outputs
        
        self.out_image = array_image(self.im,2.5,97.5)
        self.out_image = self.out_image.astype(np.uint8)
               
        # possible save as 8bits jpg
        
        if self.check_8_bits.isChecked():
               
            newname8bits = os.path.join(self.image_folder, f'{basename}_{self.nm}_8bits_r={str(radius_value)}{blur}{exagere}.jpg')                  
            self.im = Image.fromarray(self.out_image)
            self.im.save(newname8bits)

    def saveImageRGB(self):            
        
        ###### write results  
        # construction of the name of the file
        
        exageration_value = self.calc_param_exageration.value()
        radius_value = self.calc_param_radius_spin.value()
        blur_value = self.prep_blur_spin.value()
        if exageration_value == 1:        
            exagere = ''
        else:
            exagere = '_Z-exag='+str(exageration_value)
        
        if self.check_blur.isChecked():
            blur = '_smooth_r='+str(blur_value)
        else:
            blur = ''

        basename = os.path.splitext(self.image_name)[0]
        self.newname = os.path.join(self.image_folder, f'{basename}_{self.nm}_r={radius_value}{blur}{exagere}.tif')

        
        # Construction of visual outputs  
        
        r_channel = array_image(self.out_vo,2.5,97.5)
        g_channel = array_image(self.out_vop,2.5,97.5)
        b_channel = array_image(self.out_von,2.5,97.5)
        self.out_image = np.dstack((r_channel, g_channel, b_channel))
        self.out_image = self.out_image.astype(np.uint8)
        
               
        #  save as 8bits RGB jpg
        if self.check_8_bits.isChecked():
            
            newname8bits = os.path.join(self.image_folder, f'{basename}_{self.nm}_8bits_r={str(radius_value)}{blur}{exagere}.jpg')                  
            im = Image.fromarray(self.out_image, mode='RGB')
            im.save(newname8bits)
            
                  
class Calculation(QThread):

    
    countChanged = pyqtSignal(int)
    collect = pyqtSignal()

    
    def __init__(self, grid, d, h_vert_cor,
                 image_final_npy, checked_vop, checked_von, checked_cuda):
        
        super(Calculation, self).__init__()
        
        self.h_vert_cor = h_vert_cor
        self.d = d
        self.grid = grid
        self.image_final_npy = image_final_npy
        self.checked_von = checked_von
        self.checked_vop = checked_vop
        self.use_cuda = checked_cuda

        
    def run(self):
        if CUDA_AVAILABLE and self.use_cuda:
            self.run_on_cuda()
        else:
            self.run_on_cpu()
        
    def run_on_cpu(self):
       
        #calculation of VO, VOP, VON  
        
        start_time = time.time()
                
        # construction of empty arrays to receive the results of treatments
        
        self.large_vo = np.zeros((self.image_final_npy.shape[0], self.image_final_npy.shape[1], 2))   
        
        if (self.checked_vop or self.checked_von):
            self.large_vop = np.zeros((self.image_final_npy.shape[0], self.image_final_npy.shape[1], 2))   
            
        if self.checked_von:
            self.large_von = np.zeros((self.image_final_npy.shape[0], self.image_final_npy.shape[1], 2))
    
        # calculation of VO, VOP and VON
        
        for i in range(0, self.d.size):

            M = (np.roll(self.image_final_npy, [self.grid[i, 0], self.grid[i, 1]], axis = [0, 1]) - self.image_final_npy)
            vop = self.h_vert_cor[i] - M                       
            vop[vop > 2 * self.h_vert_cor[i]] = 2 * self.h_vert_cor[i]
            vop[vop < 0] = 0
            self.large_vo[:,:,1] = vop
            self.large_vo[:,:,0]  = np.sum(self.large_vo, axis=2)  #cumulative sum
            
            if (self.checked_vop or self.checked_von):
                
                svf_vop = np.copy(vop) 
                svf_vop[svf_vop > self.h_vert_cor[i]] = self.h_vert_cor[i]
                self.large_vop[:,:,1] = svf_vop
                self.large_vop[:,:,0] = np.sum(self.large_vop, axis=2)  #cumulative sum
            
            if self.checked_von:
                
                self.large_von[:,:,0] = - (self.large_vo[:,:,0] - self.large_vop[:,:,0] - self.h_vert_cor.sum()) #von obtained by subtraction
       
            self.countChanged.emit(i) #progression of the bar
            
        print(self.tr("Temps d execution : {} secondes ---").format(time.time() - start_time))
        self.quit()

    def run_on_cuda(self):
       
        #calculation of VO, VOP, VON  
        
        start_time = time.time()
                
        # construction of empty arrays to receive the results of treatments
        image_final_cpy = cp.asarray(self.image_final_npy)
        
        self.large_vo = cp.zeros((image_final_cpy.shape[0], image_final_cpy.shape[1], 2))  
        cp.cuda.Stream.null.synchronize()
        
        if (self.checked_vop or self.checked_von):
            self.large_vop = cp.zeros((image_final_cpy.shape[0], image_final_cpy.shape[1], 2))   
            cp.cuda.Stream.null.synchronize()
            
        if self.checked_von:
            self.large_von = cp.zeros((image_final_cpy.shape[0], image_final_cpy.shape[1], 2))
            cp.cuda.Stream.null.synchronize()
    
        # calculation of VO, VOP and VON
        
        
        
        for i in range(0, self.d.size):

            M = (cp.roll(image_final_cpy, [self.grid[i, 0], self.grid[i, 1]], axis = [0, 1]) - image_final_cpy)
            cp.cuda.Stream.null.synchronize()
            vop = self.h_vert_cor[i] - M  
                     
            vop[vop > 2 * self.h_vert_cor[i]] = 2 * self.h_vert_cor[i]
            vop[vop < 0] = 0
            self.large_vo[:,:,1] = vop
            self.large_vo[:,:,0]  = cp.sum(self.large_vo, axis=2)  #cumulative sum
            cp.cuda.Stream.null.synchronize()
            
            if (self.checked_vop or self.checked_von):
                
                svf_vop = cp.copy(vop) 
                svf_vop[svf_vop > self.h_vert_cor[i]] = self.h_vert_cor[i]
                self.large_vop[:,:,1] = svf_vop
                self.large_vop[:,:,0] = cp.sum(self.large_vop, axis=2)  #cumulative sum
                cp.cuda.Stream.null.synchronize()
            
            if self.checked_von:
                
                self.large_von[:,:,0] = - (self.large_vo[:,:,0] - self.large_vop[:,:,0] - self.h_vert_cor.sum()) #von obtained by subtraction
                cp.cuda.Stream.null.synchronize()
                
            self.countChanged.emit(i)
            
        print(self.tr("Temps d execution : {} secondes ---").format(time.time() - start_time))
        self.quit()
           
class PopupWin(QWidget):
    
    def __init__(self, im, shift, name):
        
        super(PopupWin, self).__init__()
        
        # popup result window (900x900 in size)
        
        Win = 900
        
        image8bits = array_image(im, 2.5,97.5)
        im8 = Image.fromarray(image8bits)
        imQt = QImage(ImageQt.ImageQt(im8))
        pixmap = QPixmap.fromImage(imQt)
        pixmap = pixmap.scaled(Win, Win, Qt.KeepAspectRatio)  
                
        self.setWindowTitle(name)
        self.setFixedSize(Win, Win)
        
        layout = QHBoxLayout()
        label_image = QLabel(self)
        label_image.setPixmap(pixmap)
        label_image.setAlignment(Qt.AlignCenter)
        layout.addWidget(label_image)
        self.setLayout(layout)
        self.move(shift, shift)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)


class PopupWinRGB(QWidget):
    
    def __init__(self, im, shift, name):
        
        super(PopupWinRGB, self).__init__()
        
        # popup result window (900x900 in size)
        
        Win = 900
        
        height, width, channel = im.shape
        bytesPerLine = 3 * width
        qImg = QImage(im.data, width, height, bytesPerLine, QImage.Format_RGB888)

        pixmap = QPixmap.fromImage(qImg)
        pixmap = pixmap.scaled(Win, Win, Qt.KeepAspectRatio)  
                
        self.setWindowTitle(name)
        self.setFixedSize(Win, Win)
        
        layout = QHBoxLayout()
        label_image = QLabel(self)
        label_image.setPixmap(pixmap)
        label_image.setAlignment(Qt.AlignCenter)
        layout.addWidget(label_image)
        self.setLayout(layout)
        self.move(shift, shift)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        

def blurring(in_array, size):

    # Blur using fft
    
    padded_array = np.pad(in_array, size, 'symmetric')
    x, y = np.mgrid[-size:size + 1, -size:size + 1]
    g = np.exp(-(x**2 / float(size) + y**2 / float(size)))
    g = (g / g.sum()).astype(in_array.dtype)
    
    return fftconvolve(padded_array, g, mode='valid')
      


def tranformImage(image):
    
    # transform DEM in 32 bits and put Nan in place of +/- 32767
    
    image = np.float32(image)
    image[image == -32767] = 'nan'
    image[image == 32767] = 'nan'
    wherenan = np.isnan(image)
    image_final = image
    
    return wherenan, image_final # return the palce an array witht the position of Nan and the transformed DEM
        

def array_image(image_input, p1, p2):
    
    # contrast improvement within the p1 - p2 percentiles (truncate)
    
    percentile = np.nanpercentile(image_input, [p1, p2])
    
    image_input = np.nan_to_num(image_input)
    image_input[image_input < percentile[0]] = percentile[0]
    image_input[image_input > percentile[1]] = percentile[1]
    
    image_input = image_input - np.min(image_input)
    image_input = image_input / np.max(image_input)
    image_input = 255 * image_input
    out = image_input.astype(np.uint8)
    
    return out # return the contrasted image
        



def main():
    app = QApplication(sys.argv)
    locale = QLocale.system().name()
    print(locale)
    qtTranslator = QTranslator()
    if qtTranslator.load("qt_" + locale, ":/"):
        app.installTranslator(qtTranslator)
    appTranslator = QTranslator()
    if appTranslator.load("vSky_" + locale, "./"):
        app.installTranslator(appTranslator)
    win = MainWindow()
    win.show()
    return app.exec_()

if __name__ == '__main__':
    sys.exit(main()) 
